patterns:
    $goodTime = (~добрый (~утро|~день|~вечер|~ночь))
    $bot = (бот|робот*|чатбот|чат бот|чат-бот|чатик|~чат)
    $hi = (привет*/здравствуй*)
    $phone = $regexp<79\d{9}>
    $City = $entity<Cities> || converter = function(parseTree) {var id = parseTree.Cities[0].value; return Cities[id].value;};

