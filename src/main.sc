require: slotfilling/slotFilling.sc
  module = sys.zb-common

require: localPatterns.sc

require: dicts/cities.csv
    name = Cities
    var = Cities

require: dicts/discount.yaml
    var = discountInfo
    
# require: functions.js

init:
    bind("postProcess", function($context){
        $context.session.lastState = $context.currentState;
        log(toPrettyString($context.session) + "@@@@");
    });

init:
    bind("postProcess", function($context){
        $context.response.client = $context.client;
    });


theme: /
    state: Welcome
        q!: *start
        q!: $hi *
        script:
            $response.replies = $response.replies || [];
            $response.replies.push( {
               type: "image",
               imageUrl: "https://cdn.pixabay.com/photo/2017/06/05/11/01/airport-2373727_960_720.jpg",
               text: "Самолет"
            });
        random:
            a: Здравствуйте!
            a: Приветствую!
        a: Меня зовут {{ $injector.botName }}.
        go!: /Service/SuggestHelp


    state: CatchAll || noContext = true
        event!: noMatch
        a: Простите, я не поняла. Переформулируйте, пожалуйста, свой запрос.


theme: /Service
    state: SuggestHelp
        q: отмена || fromState = /Phone/Ask, onlyThisState = true
        a: Давайте я помогу вам купить билет на самолет, хорошо?

        buttons:
            "Да"
            "Нет"

        state: Accepted
            intent!: /BuyTicket
            q: * (да/давай*/хорошо) *
            a: Отлично!

            if: $client.phone
               go!: /Phone/Confirm
            else:
                go!: /Phone/Ask

        state: Rejected
            q: * (не/нет) *
            a: Боюсь, что ничего другого я пока предложить не могу...


theme: /Phone
    state: Ask || modal = true
        a: Для продолжения напишите, пожалуйста, мне ваш номер в формате 79000000000.

        buttons:
            "Отмена"

        state: Get
            q: $phone
            go!: /Phone/Confirm

        state: Wrong
            q: *
            a: Что-то не похоже на номер телефона...
            go!: /Phone/Ask


    state: Confirm
        script:
            $temp.phone = $parseTree._phone || $client.phone;
        a: Ваш номер {{ $temp.phone }}, верно?
        script:
            $session.probablyPhone = $temp.phone;
        buttons:
            "Да"
            "Нет"

        state: Yes
            q: (да/верно)
            script:
                $client.phone = $session.probablyPhone;
                delete $session.probablyPhone;
            a: Хорошо.
            go!: /Discount/Inform

        state: No
            q: (нет/не [верно])
            go!: /Phone/Ask


theme: /Discount
    state: Inform
        script:
            var nowDayOfWeek = $jsapi.dateForZone("Europe/Moscow", "EEEE");
            var discount = discountInfo[nowDayOfWeek];
            
            if (discount) {
                var nowDate = $jsapi.dateForZone("Europe/Moscow", "dd.MM");
                var answerText = "Хочу отметить, что вам крупно повезло! Сегодня (" + nowDate + ") действует акция!";

                $reactions.answer(answerText);
                $reactions.answer(discount);
            }
        go!: /City/Departure


theme: /City
    state: Departure
        a: Назовите, пожалуйста, город отправления.
        
        state: Get
            q: * $City *
            script:
                $session.departureCity = $parseTree._City.name;
            a: Итак, город отправления: {{ $session.departureCity }}.
            go!: /City/Arrival

    state: Arrival
        a: Назовите, пожалуйста, город прибытия.

        state: Get
            q: * $City *
            script:
                $session.arrivalCity = $parseTree._City.name;
                $session.arrivalCoordinates = {
                    lat: $parseTree._City.lat,
                    lon: $parseTree._City.lon
                };
            a: Отправляемся в город {{ $session.arrivalCity }}.
            a: Координаты города в который мы направляемся: lat {{ $session.arrivalCoordinates.lat }} lon {{ $session.arrivalCoordinates.lon }}











































